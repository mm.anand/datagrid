// ignore_for_file: deprecated_member_use

import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

// ignore: prefer_const_constructors
void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool isButtonPressed = false;
  List _data2disuser = [];

  @override
  void initState() {
    super.initState();
    readconfigJson();
    DistrictList();
  }

  var _district = ['Tenkasi', 'chennai'];
  var _currentItemSelected = 'Tenkasi';
  List _data = [];
  var districtcode;
  DistrictList() async {
    print("state code");
    http.Response responsedis = await http.post(
        Uri.parse("https://lgdirectory.gov.in/webservices/lgdws/districtList"),
        body: {'stateCode': "33"});
    var response1 = responsedis.body;
    print(response1);
    List datalistjson = jsonDecode(response1);
    print(datalistjson);
    setState(() {
      _data2disuser = datalistjson;
      print("data2user:${_data2disuser.toString()}");
    });
  }

  // Fetch content from the json file
  Future<void> readconfigJson() async {
    final String response = await rootBundle.loadString('assets/config.json');
    final data = await json.decode(response);
    setState(() {
      print("AppBar Heading:" + data["form1"][0]["app_heading"]);
      print("Heading:" + data["form1"][0]["heading"]);
      print("TextField 1 is: " + data["form1"][0]["textfield1"]);
      print("TextField 2 is: " + data["form1"][0]["textfield2"]);
      print("TextField 3 is: " + data["form1"][0]["textfield3"]);
      print("Button is: " + data["form1"][0]["Button"]);
      _data = data["form1"];
    });
  }

  @override
  Widget build(BuildContext context) {
    //  final appTitle = 'Flutter Form Demo';
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        //  title: appTitle,
        home: Scaffold(
            appBar: AppBar(
              title: Text(
                  _data.isNotEmpty ? _data[0]["app_heading"].toString() : ""),
            ),
            body: Container(
              child: SingleChildScrollView(
                child: Container(
                  // ignore: prefer_const_constructors
                  padding: EdgeInsets.all(60.0),
                  child: Form(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            _data.isNotEmpty
                                ? _data[0]["heading"].toString()
                                : "",
                            style: const TextStyle(
                                fontSize: 24, fontWeight: FontWeight.w800),
                          ),
                          TextFormField(
                              decoration: InputDecoration(
                            label: Text(_data.isNotEmpty
                                ? _data[0]["textfield1"].toString()
                                : ""),
                          )),
                          TextFormField(
                            decoration: InputDecoration(
                                label: Text(_data.isNotEmpty
                                    ? _data[0]["textfield2"].toString()
                                    : "")),
                          ),
                          SizedBox(height: 25),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "District",
                                textAlign: TextAlign.start,
                                style: TextStyle(fontSize: 16),
                              ),
                              SizedBox(
                                width: 30,
                              ),
                              DropdownButton(
                                items: _data2disuser.map((dropDownStringItem) {
                                  return DropdownMenuItem(
                                    value: dropDownStringItem['districtCode'],
                                    child: Text(dropDownStringItem[
                                        'districtNameEnglish']),
                                  );
                                }).toList(),
                                onChanged: (newValueSelected) {
                                  setState(() {
                                    districtcode = newValueSelected;
                                  });
                                },
                                value: districtcode,
                              ),
                            ],
                          ),
                          TextFormField(
                            decoration: InputDecoration(
                              label: Text(_data.isNotEmpty
                                  ? _data[0]["textfield3"].toString()
                                  : ""),
                            ),
                            maxLines: 3,
                          ),
                          const SizedBox(height: 20),
                          RaisedButton(
                              child: Text(_data.isNotEmpty
                                  ? _data[0]["Button"].toString()
                                  : ""),
                              onPressed: () {
                                setState(() {
                                  isButtonPressed = true;
                                });
                                print(" form submitted successfully");
                              }),
                          (isButtonPressed == true)
                              ? const Text('Form Submitted Succesfully')
                              : Text('')
                        ]),
                  ),
                ),
              ),
            )));
  }
}
